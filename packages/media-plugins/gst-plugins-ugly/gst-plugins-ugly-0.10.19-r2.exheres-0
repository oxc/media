# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2009, 2011 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A set of well-supported plugins with questionable licenses"
HOMEPAGE="http://gstreamer.freedesktop.org/"
DOWNLOADS="http://gstreamer.freedesktop.org/src/${PN}/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="0.10"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    gstreamer_plugins:
        a52      [[ description = [ ATSC A/52 audio decoding using a52dec ] ]]
        cdio     [[ description = [ Read audio CDs using libcdio ] ]]
        dvdread  [[ description = [ Access DVD title/chapter/angle using libdvdread ] ]]
        h264     [[ description = [ H.264/AVC/MPEG-4 part 10 encoding using the x264 library ] ]]
        lame     [[ description = [ MP3 audio encoding using LAME ] ]]
        mad      [[ description = [ MP3 audio decoding using mad ] ]]
        mp2      [[ description = [ MP2 audio encoding using TwoLAME ] ]]
        mpeg2    [[ description = [ MPEG video stream decoding using libmpeg2 ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
        dev-util/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.3] )
    build+run:
        media-libs/gstreamer:0.10[>=0.10.36]
        media-plugins/gst-plugins-base:0.10[>=0.10.36]
        dev-libs/glib:2[>=2.24]
        dev-libs/orc:0.4[>=0.4.11]
        gstreamer_plugins:a52? ( media-libs/a52dec )
        gstreamer_plugins:cdio? ( dev-libs/libcdio[>=0.90] )
        gstreamer_plugins:dvdread? ( media-libs/libdvdread )
        gstreamer_plugins:h264? ( media-libs/x264[>=0.55.0] )
        gstreamer_plugins:lame? ( media-sound/lame )
        gstreamer_plugins:mad? ( media-libs/libmad[>=0.15] )
        gstreamer_plugins:mp2? ( media-libs/twolame[>=0.3.10] )
        gstreamer_plugins:mpeg2? ( media-libs/libmpeg2[>=0.4.0] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/6700410efb1786e5a40f45515574a1bf713ba011.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-experimental'

    '--enable-asfdemux'
    '--enable-dvdlpcmdec'
    '--enable-dvdsub'
    '--enable-iec958'
    '--enable-mpegaudioparse'
    '--enable-mpegstream'
    '--enable-realmedia'
    '--enable-synaesthesia'

    '--disable-amrnb'
    '--disable-amrwb'
    '--disable-sidplay'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc gtk-doc'

    'gstreamer_plugins:a52 a52dec'
    'gstreamer_plugins:cdio'
    'gstreamer_plugins:dvdread'
    'gstreamer_plugins:h264 x264'
    'gstreamer_plugins:lame'
    'gstreamer_plugins:mad'
    'gstreamer_plugins:mp2 twolame'
    'gstreamer_plugins:mpeg2 mpeg2dec'
)

