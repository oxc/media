# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2009, 2011, 2013 Marvin Schmidt <marv@exherbo.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A set of well-supported plugins with questionable licenses"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    doc
    gstreamer_plugins:
        a52      [[ description = [ ATSC A/52 audio decoding using a52dec ] ]]
        amr      [[ description = [ Adaptive Multi Rate encoder/decoder (narrow band, wide band) ] ]]
        cdio     [[ description = [ Read audio CDs using libcdio ] ]]
        dvdread  [[ description = [ Access DVD title/chapter/angle using libdvdread ] ]]
        h264     [[ description = [ H.264/AVC/MPEG-4 part 10 encoding using the x264 library ] ]]
        mpeg2    [[ description = [ MPEG video stream decoding using libmpeg2 ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.16]
        media-libs/gstreamer:1.0[>=${PV}]
        media-plugins/gst-plugins-base:1.0[>=${PV}]
        gstreamer_plugins:a52? ( media-libs/a52dec )
        gstreamer_plugins:amr? ( media-libs/opencore-amr[>=0.1.3] )
        gstreamer_plugins:cdio? ( dev-libs/libcdio[>=0.76] )
        gstreamer_plugins:dvdread? ( media-libs/libdvdread )
        gstreamer_plugins:h264? ( media-libs/x264[>=0.120] )
        gstreamer_plugins:mpeg2? ( media-libs/libmpeg2[>=0.5.1] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-experimental'

    '--enable-asfdemux'
    '--enable-dvdlpcmdec'
    '--enable-dvdsub'
    '--enable-realmedia'
    '--enable-xingmux'

    # Missing libsidplay
    '--disable-sidplay'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'debug'
    'doc gtk-doc'

    'gstreamer_plugins:a52 a52dec'
    'gstreamer_plugins:amr amrnb'
    'gstreamer_plugins:amr amrwb'
    'gstreamer_plugins:cdio'
    'gstreamer_plugins:dvdread'
    'gstreamer_plugins:h264 x264'
    'gstreamer_plugins:mpeg2 mpeg2dec'
)

