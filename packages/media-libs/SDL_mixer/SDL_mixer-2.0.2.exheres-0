# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Copyright 2010-2015 Wulf C: Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

MY_PNV=SDL2_mixer-${PV}

SUMMARY="Multichannel audio mixer library for SDL"
DESCRIPTION="
SDL_mixer is a simple multi-channel audio mixer. It supports 8 channels of 16 bit
stereo audio, plus a single channel of music.
"
HOMEPAGE="https://www.libsdl.org/projects/SDL_mixer"
DOWNLOADS="${HOMEPAGE}/release/${MY_PNV}.tar.gz"

LICENCES="ZLIB"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    flac [[ description = [ enable FLAC music ] ]]
    midi [[ description = [ enable MIDI music via timidity ] ]]
    mikmod [[ description = [ support mod files with libmikmod ] ]]
    ogg
    (
        mad [[ description = [ support mp3 files with libmad ] ]]
        mpg123 [[ description = [ support mp3 files with mpg123 ] ]]
        smpeg [[ description = [ support mp3 files with smpeg ] ]]
    ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build+run:
        media-libs/SDL:2[>=2.0.7]
        flac? ( media-libs/flac )
        mad? ( media-libs/libmad )
        midi? ( media-sound/timidity++ )
        mikmod? ( media-libs/libmikmod )
        mpg123? ( media-sound/mpg123 )
        ogg? ( media-libs/libvorbis )
        smpeg? ( media-libs/smpeg:2 )
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-music-cmd
    --enable-music-midi-native
    --enable-music-wave
    --disable-music-midi-fluidsynth
    --disable-music-mod-modplug
    --disable-music-ogg-tremor
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "flac music-flac"
    "flac music-flac-shared"
    "mad music-mp3-mad-gpl"
    "mad music-mp3-mad-gpl-dithering"
    "midi music-midi"
    "midi music-midi-timidity"
    "mikmod music-mod-mikmod"
    "mikmod music-mod-mikmod-shared"
    "mpg123 music-mp3-mpg123"
    "mpg123 music-mp3-mpg123-shared"
    "ogg music-ogg"
    "ogg music-ogg-shared"
    "smpeg music-mp3-smpeg"
    "smpeg music-mp3-smpeg-shared"
)

# parallel build has a race condition
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

AT_M4DIR=( acinclude )

src_prepare() {
    default

    # fixes libtool related build failures
    edo rm aclocal.m4
    edo rm acinclude/l*.m4

    eautoreconf
}

