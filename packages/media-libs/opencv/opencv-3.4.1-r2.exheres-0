# Copyright 2010 Yury G. Kudryashov <urkud@ya.ru>
# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'opencv-2.0.0-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

MY_PN=OpenCV

require cmake [ api=2 ] \
    flag-o-matic \
    github \
    python [ has_lib=false ]

SUMMARY="A collection of algorithms and sample code for various computer vision problems"
HOMEPAGE="https://${PN}.org"
# 460 MiB
#DOWNLOADS+=" https://github.com/Itseez/${PN}_extra/archive/${PV}.tar.gz -> ${PN}_extra-${PV}"
DOWNLOADS+="
    contrib? ( https://github.com/${PN}/${PN}_contrib/archive/${PV}.tar.gz -> ${PN}_contrib-${PV}.tar.gz )
"

REMOTE_IDS="sourceforge:${PN}library"

LICENCES="
    BSD-3
    xine? ( GPL-2 )
"
SLOT="0"
PLATFORMS="~amd64 ~x86"

# All listed in modules/contrib_world/CMakeLists.txt minus xfeatures2d, which
# contains no-free parts.
contrib_modules="bgsegm bioinspired ccalib cvv face line_descriptor optflow
    reg rgbd saliency surface_matching text tracking ximgproc xobjdetect
    xphoto
"
MYOPTIONS="
    camera [[ description = [ Capture using your camera device via libgphoto2 ] ]]
    contrib [[ description = [ Extra modules, possibly without stable API and not well-tested ] ]]
    doc
    examples
    ffmpeg
    gstreamer
    gtk
    ieee1394
    java
    jpeg2000
    opencl [[ description = [ Hardware acceleration via OpenCL ] ]]
    openexr
    opengl
    openmp [[ description = [ Support for Open Multi-Processin ] ]]
    python
    qt5 [[ description = [ Adds support for the Qt GUI/Application Toolkit version 5.x ] ]]
    threads
    tiff
    v4l
    vtk [[ description = [ Build Viz module allowing 3D visualization via VTK ] ]]
    webp [[ description = [ Support for the WebP image format ] ]]
    xine

    ( contrib_modules: ( ${contrib_modules} ) [[ *requires = contrib ]] )
    contrib? ( contrib_modules: ( ${contrib_modules} )
        [[ number-selected = at-least-one ]]
    )

    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
    opengl? ( ( gtk qt5 ) [[ number-selected = at-least-one ]] )

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# FIXME: Download this data if tests are enabled and run tests
RESTRICT=test

DEPENDENCIES="
    build:
        sci-libs/eigen:3 [[ note = [ Optional, enable since this is a build-time header-only dependency. ] ]]
        sys-libs/zlib[>=1.2.3]
        doc? (
            dev-python/Sphinx
            dev-texlive/texlive-latex
        )
        gtk? ( virtual/pkg-config )

    build+run:
        media-libs/libpng:=
        camera? ( media-libs/libgphoto2 )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        gtk? (
            dev-libs/glib:2 [[ note = gthread ]]
            x11-libs/gtk+:3
        )
        ieee1394? (
            media-libs/libdc1394:2
            media-libs/libraw1394[>=1.2.0]
        )
        java? (
            dev-java/apache-ant
            virtual/jdk:=[>=1.6] [[ note = [ recommended version spec ] ]]
        )
        jpeg2000? ( media-libs/jasper )
        opencl? ( dev-libs/opencl-headers )
        openexr? (
            media-libs/ilmbase
            media-libs/openexr
        )
        opengl? (
            x11-dri/glu
            x11-dri/mesa
            gtk? ( x11-libs/gtkglext )
        )
        openmp? ( sys-libs/libgomp:* )
        python? ( dev-python/numpy )
        qt5? ( x11-libs/qtbase:5 )
        threads? ( dev-libs/tbb )
        tiff? ( media-libs/tiff )
        v4l? ( media-libs/v4l-utils )
        vtk? ( sci-libs/vtk[>=5.8.0][qt5=] )
        webp? ( media-libs/libwebp:= )
        xine? ( media-libs/xine-lib )
        contrib_modules:text? ( app-text/tesseract )
        providers:ffmpeg? ( media/ffmpeg )
        providers:ijg-jpeg? ( media-libs/jpeg )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libav? ( media/libav )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/549b5df22520b60b91dd77096434d79425b31ac2.patch
)

pkg_setup() {
    # Force -DNDEBUG because upstream has errors in their debug code
    append-flags -DNDEBUG
}

src_configure() {
    # FIXME: ipp support? (library is non-free / costs money)
    # FIXME: unicap support
    local p cmakeparams=(
        -DBUILD_IPP_IW:BOOL=FALSE
        -DBUILD_ITT:BOOL=FALSE
        -DBUILD_JASPER:BOOL=FALSE
        -DBUILD_JPEG:BOOL=FALSE
        -DBUILD_OPENEXR:BOOL=FALSE
        -DBUILD_PNG:BOOL=FALSE
        -DBUILD_PROTOBUF:BOOL=FALSE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DBUILD_TBB:BOOL=FALSE
        -DBUILD_TIFF:BOOL=FALSE
        -DBUILD_WEBP:BOOL=FALSE
        -DBUILD_ZLIB:BOOL=FALSE
        -DBUILD_opencv_dnn:BOOL=FALSE
        -DCMAKE_SKIP_RPATH:BOOL=FALSE
        -DOPENCV_DOC_INSTALL_PATH:PATH=/usr/share/doc/${PNVR}
        -DOPENCV_OTHER_INSTALL_PATH:PATH=/usr/share/OpenCV
        -DOPENCV_SAMPLES_SRC_INSTALL_PATH:PATH=/usr/share/OpenCV/samples
        -DOPENCV_TEST_DATA_INSTALL_PATH:PATH=/usr/share/OpenCV/testdata
        -DPKG_CONFIG_EXECUTABLE:PATH=${PKG_CONFIG}
        -DENABLE_CCACHE:BOOL=FALSE
        -DENABLE_DYNAMIC_CUDA:BOOL=FALSE
        -DENABLE_OMIT_FRAME_POINTER:BOOL=FALSE
        -DENABLE_PRECOMPILED_HEADERS:BOOL=FALSE
        -DENABLE_PROFILING:BOOL=FALSE
        -DWITH_ARAVIS:BOOL=FALSE
        -DWITH_CAROTENE:BOOL=FALSE
        -DWITH_CPUFEATURES:BOOL=FALSE
        -DWITH_CUDA:BOOL=FALSE
        -DWITH_EIGEN:BOOL=TRUE
        -DWITH_GDCM:BOOL=FALSE
        -DWITH_GIGEAPI:BOOL=FALSE
        -DWITH_HALIDE:BOOL=FALSE
        -DWITH_INF_ENGINE:BOOL=FALSE
        -DWITH_INTELPERC:BOOL=FALSE
        -DWITH_IPP:BOOL=FALSE
        -DWITH_ITT:BOOL=FALSE
        -DWITH_JPEG:BOOL=TRUE
        -DWITH_LAPACK:BOOL=FALSE
        -DWITH_MATLAB:BOOL=FALSE
        -DWITH_MFX:BOOL=FALSE
        -DWITH_OPENVX:BOOL=FALSE
        -DWITH_PNG:BOOL=TRUE
        -DWITH_PROTOBUF:BOOL=FALSE
        -DWITH_PVAPI:BOOL=FALSE
        -DWITH_UNICAP:BOOL=FALSE
        -DWITH_GTK_2_X:BOOL=FALSE
        $(cmake_build doc DOCS)
        $(cmake_build EXAMPLES)
        $(cmake_build java JAVA)
        $(cmake_build python NEW_PYTHON_SUPPORT)
        $(cmake_option examples INSTALL_C_EXAMPLES)
        $(cmake_with camera GPHOTO2)
        $(cmake_with FFMPEG)
        $(cmake_with GSTREAMER)
        $(cmake_with GTK)
        $(cmake_with ieee1394 1394)
        $(cmake_with JAVA)
        $(cmake_with jpeg2000 JASPER)
        $(cmake_with OPENCL)
        $(cmake_with OPENEXR)
        $(cmake_with OPENGL)
        $(cmake_with OPENMP)
        $(cmake_with qt5 5 FALSE)
        $(cmake_with threads TBB)
        $(cmake_with TIFF)
        $(cmake_with V4L)
        $(cmake_with VTK)
        $(cmake_with WEBP)
        $(cmake_with XINE)
        $(expecting_tests -DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE)
    )

    if option contrib ; then
        cmakeparams+=(
            -DOPENCV_EXTRA_MODULES_PATH="${WORKBASE}"/opencv_contrib-${PV}/modules
            -DBUILD_opencv_aruco:BOOL=FALSE
            -DBUILD_opencv_cnn_3dobj:BOOL=FALSE
            -DBUILD_opencv_datasets:BOOL=FALSE
            -DBUILD_opencv_dnn_modern:BOOL=FALSE
            -DBUILD_opencv_dnn_objdetect:BOOL=FALSE
            -DBUILD_opencv_dnns_easily_fooled:BOOL=FALSE
            -DBUILD_opencv_dpm:BOOL=FALSE
            -DBUILD_opencv_freetype:BOOL=FALSE
            -DBUILD_opencv_fuzzy:BOOL=FALSE
            -DBUILD_opencv_hdf:BOOL=FALSE
            -DBUILD_opencv_matlab:BOOL=FALSE
            -DBUILD_opencv_ovis:BOOL=FALSE
            -DBUILD_opencv_plot:BOOL=FALSE
            -DBUILD_opencv_sfm:BOOL=FALSE
            -DBUILD_opencv_stereo:BOOL=FALSE
            -DBUILD_opencv_structured_light:BOOL=FALSE
            -DBUILD_opencv_xfeatures2d:BOOL=FALSE
        )
        local module
        for module in ${contrib_modules} ; do
            cmakeparams+=(
                $(cmake_build contrib_modules:${module} opencv_${module})
            )
        done
    fi

    ecmake "${cmakeparams[@]}"
}

src_test() {
    # see notice about tests above
    for unittest in $(find bin/ -type f -name "opencv_test_*") ; do
        edo ./${unittest}
    done
}

src_install() {
    cmake_src_install

    # Add a hack until we figure out how to configure this properly via CMake.
    if [[ -d ${IMAGE}/usr/lib/python$(python_get_abi) ]]; then
        edo mv "${IMAGE}"/usr/{lib,${LIBDIR}}/python$(python_get_abi)
        edo find "${IMAGE}"/usr/lib -type d -empty -delete
        python_bytecompile
    fi
}

