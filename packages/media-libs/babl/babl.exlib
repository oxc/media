# Copyright 2009, 2011, 2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'babl-0.0.22.ebuild' from Gentoo, which is:
#    Copyright 1999-2008 Gentoo Foundation

export_exlib_phases src_configure

SUMMARY="A dynamic, any to any, pixel format conversion library"
HOMEPAGE="http://www.gegl.org/${PN}/"

LICENCES="LGPL-3"
SLOT="0"
MYOPTIONS="
    ( amd64_cpu_features:
        sse3
        sse4.1
        f16c
    )
    ( x86_cpu_features:
        mmx
        sse
        sse2
        sse3
        sse4.1
        f16c
    )
    ( platform:
        amd64
    )
"

DEPENDENCIES=""

# ./configure checks for w3m, rsvg, but those are only used for make dist
babl_src_configure() {
    local myconf=()

    # If I don't do this, they are forced off
    if option platform:amd64 ; then
        myconf+=(
            --enable-mmx
            --enable-sse
            --enable-sse2
            $(option_enable amd64_cpu_features:sse3)
            $(option_enable amd64_cpu_features:sse4.1 sse4_1)
            $(option_enable amd64_cpu_features:f16c f16c)
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:mmx)
            $(option_enable x86_cpu_features:sse)
            $(option_enable x86_cpu_features:sse2)
            $(option_enable x86_cpu_features:sse3)
            $(option_enable x86_cpu_features:sse4.1 sse4_1)
            $(option_enable x86_cpu_features:f16c f16c)
        )
    fi

    econf "${myconf[@]}"
}

