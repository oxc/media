# Copyright 2009 Adriaan Leijnse <adriaan@leijnse.net>
# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU Public License v2

require github [ user=jackaudio pn=jack2 release=v${PV} suffix=tar.gz ] waf

SUMMARY="JACK is a low-latency audio server"
DESCRIPTION="
JACK is a low-latency audio server, written for any operating system that is reasonably POSIX
compliant. It currently exists for Linux, OS X, Solaris, FreeBSD and Windows. It can connect several
client applications to an audio device, and allow them to share audio with each other. Clients can
run as separate processes like normal applications, or within the JACK server as \"plugins\".
JACK was designed from the ground up for professional audio work, and its design focuses on two key
areas: synchronous execution of all clients, and low latency operation.
"
HOMEPAGE+=" http://jackaudio.org"

BUGS_TO="adriaan@leijnse.net"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    alsa [[ description = [ Build the ALSA driver ] ]]
    dbus [[ description = [ Build the JACK2 D-Bus interface. \
                            This option disables the classic jackd interface ] ]]
    debug
    doc [[ description = [ Build API documentation ] ]]
    firewire [[ description = [ Build the FFADO firewire driver ] ]]
"

# No function test defined in wscript
RESTRICT="test"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
    build+run:
        media-libs/celt:0.11
        media-libs/libsamplerate
        media-libs/libsndfile
        media-libs/opus[>=0.9.0]
        alsa? ( sys-sound/alsa-lib[>=1.0.18] )
        dbus? (
            dev-libs/expat
            sys-apps/dbus[>=1.0.0]
        )
        firewire? ( media-libs/libffado[>=1.999.17] )
"

WORK=${WORKBASE}/jack2-${PV}

WAF_SRC_CONFIGURE_PARAMS=(
    --hates={data,sbin}dir
    --mandir=/usr/share/man/man1
    --enable-pkg-config-dbus-service-dir
    --celt=yes
    --freebob=no
    --iio=no
    --opus=yes
    --portaudio=no
    --readline=yes
    --samplerate=yes
    --sndfile=yes
    --winmme=no
)

src_configure () {
    WAF_SRC_CONFIGURE_PARAMS+=(
        $(option alsa '--alsa=yes' '--alsa=no')
        $(option debug '--debug')
        $(option dbus '--autostart=dbus' '--autostart=classic')
        $(option dbus '--dbus' '--classic')
        $(option doc '--doxygen=yes' '--doxygen=no')
        $(option firewire '--firewire=yes' '--firewire=no')
    )

    waf_src_configure
}

