# Copyright 2012, 2013 Ali Polatel <alip@exherbo.org>
# Based in part upon qtractor-0.5.5.ebuild which is:
#   Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require qmake [ slot=5 ] \
    sourceforge [ suffix='tar.gz' ] \
    option-renames [ renames=[ 'mad mp3' 'suil lv2' ] ] \
    freedesktop-desktop \
    freedesktop-mime \
    gtk-icon-cache

SUMMARY="Audio/MIDI multi-track sequencer application"
DESCRIPTION="
Qtractor is an Audio/MIDI multi-track sequencer application written in C++ with
the Qt4 framework. Target platform is Linux, where the Jack Audio Connection Kit
(JACK) for audio, and the Advanced Linux Sound Architecture (ALSA) for MIDI, are
the main infrastructures to evolve as a fairly-featured Linux desktop audio
workstation GUI, specially dedicated to the personal home-studio.
"

BUGS_TO="alip@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    debug
    dssi
    liblo
    libsamplerate
    mp3
    rubberband
    vorbis
    lv2 [[ description = [ Enable LV2 plug-in support ] ]]
    ( platform: amd64 )
    ( x86_cpu_features: sse )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:5[>=5.1] [[ note = [ lrelease/lupdate for translations ] ]]
    build+run:
        media-libs/ladspa-sdk
        media-libs/libsndfile
        media-sound/jack-audio-connection-kit
        sys-sound/alsa-lib
        x11-libs/qtbase:5[>=5.1]
        x11-libs/qtx11extras:5[>=5.1]
        dssi? ( media-libs/dssi )
        liblo? ( media-libs/liblo )
        libsamplerate? ( media-libs/libsamplerate )
        lv2? (
            media-libs/lilv
            media-libs/suil
        )
        mp3? ( media-libs/libmad )
        rubberband? ( media-libs/rubberband )
        vorbis? ( media-libs/libvorbis )
"

src_configure() {
    local myconf=()

    myconf+=(
        --enable-ladspa
        --enable-libz
        --disable-qt4
        $(option_enable debug)
        $(option_enable dssi)
        $(option_enable liblo)
        $(option_enable libsamplerate)
        $(option_enable mp3 libmad)
        $(option_enable vorbis libvorbis)
        $(option_enable rubberband librubberband)
        $(option_enable lv2)
        $(option_enable lv2 suil)
        $(option_enable lv2 lilv)
    )

    if option platform:amd64; then
        myconf+=(
            --enable-sse
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:sse)
        )
    fi

    econf "${myconf[@]}"
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

