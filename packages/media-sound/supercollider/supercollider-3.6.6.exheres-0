# Copyright 2014 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]
require sourceforge [ suffix="tar.bz2" pnv="SuperCollider-${PV}-Source-linux" ]
require freedesktop-desktop freedesktop-mime

SUMMARY="Language for real time audio synthesis and algorithmic composition"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="curl debug doc emacs qt vim
    amd64_cpu_features: sse sse4.1 sse4.2
"

# TODO: (see below)
# - dev-libs/boost
# - dev-libs/yaml-cpp
DEPENDENCIES="
    build+run:
        dev-libs/icu
        media-libs/libsndfile
        media-sound/jack-audio-connection-kit
        sci-libs/fftw
        sys-libs/readline
        sys-sound/alsa-lib
        x11-libs/libXt
        curl? ( net-misc/curl )
        emacs? ( app-editors/emacs )
        qt? ( x11-libs/qtwebkit )
        vim? ( || ( app-editors/vim[ruby] )
                  ( app-editors/gvim[ruby] )
             )
"

BUGS_TO="alip@exherbo.org"

CMAKE_SOURCE="${WORKBASE}/SuperCollider-Source"

# 1. Build fails with system yamlcpp: dev-libs/yaml-cpp[=0.5.1] >
# /build/media-sound-supercollider-3.6.6/work/SuperCollider-Source/editors/sc-ide/core/main.cpp:32:27:
# fatal error: yaml-cpp/node.h: No such file or directory^M
 #include "yaml-cpp/node.h"^M
# 2. Test suite does not build with external boost.
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DLIBSCSYNTH=ON -DSC_ED=OFF -DSC_WII=OFF
    -DNATIVE=OFF -DLTO=OFF -DSC_DOC_RENDER=OFF
    -DNO_AVAHI=ON -DSYSTEM_BOOST=OFF -DSYSTEM_YAMLCPP=OFF
    -DSC_PLUGIN_DIR='${CMAKE_PREFIX_PATH}/SuperCollider/plugins'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'amd64_cpu_features:sse SSE'
    'amd64_cpu_features:sse4.1 SSE41'
    'amd64_cpu_features:sse4.2 SSE42'
    'curl CURL'
    'doc INSTALL_HELP'
    'debug SC_MEMORY_DEBUGGING'
    'emacs SC_EL'
    'qt SC_QT'
    'qt SC_IDE'
    'vim SC_VIM'
)

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}/${PNV}-exherbo-libdir.patch" )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( README.txt README_LINUX.txt )

