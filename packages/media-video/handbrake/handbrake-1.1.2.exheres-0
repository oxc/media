# Copyright 2013-2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'handbrake-0.9.9_pre5441-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2013 Gentoo Foundation

MY_PNV="${PNV/handbrake/HandBrake}"

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="Tool to convert video from nearly any format to modern codecs"
HOMEPAGE="https://${PN}.fr"
DOWNLOADS="https://download.${PN}.fr/releases/${PV}/${MY_PNV}-source.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk3
    hevc [[ description = [ Enable H.265/HEVC encoding using x265 ] ]]
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/nasm[>=2.13.0]
        dev-lang/python:2.7
        dev-lang/yasm[>=1.2.0]
        sys-devel/cmake
        virtual/pkg-config[>=0.9.0]
        gtk3? (
            dev-util/intltool[>=0.35.0]
            sys-devel/gettext
        )
    build+run:
        dev-libs/fribidi
        dev-libs/jansson
        dev-libs/libxml2:2.0
        media-libs/fdk-aac
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libass
        media-libs/libbluray[>=1.0.0]
        media-libs/libdvdnav
        media-libs/libdvdread
        media-libs/libogg
        media-libs/libsamplerate
        media-libs/libtheora
        media-libs/libvorbis
        media-libs/libvpx:=
        media-libs/opus
        media-libs/x264
        media-sound/lame
        x11-libs/harfbuzz
        gtk3? (
            dev-libs/glib:2
            gnome-desktop/libgudev
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:3[>=3.16]
            x11-libs/libnotify
            x11-libs/pango
        )
        hevc? ( media-libs/x265 )
        providers:ffmpeg? ( media/ffmpeg[>=2.2] )
        providers:libav?  ( media/libav[>=10.1] )
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0003-Remove-libdvdnav-duplication-and-call-it-on-the-orig.patch
)

src_prepare() {
    default

    # Get rid of leftover bundled library build definitions,
    # the version 0.9.9 supports the use of system libraries.
    edo sed -i -e 's:.*\(/contrib\|contrib/\).*::g' make/include/main.defs

    # Use the correct build tools
    export CC_FOR_BUILD=$(exhost --build)-cc
    export BUILD_PKG_CONFIG=$(exhost --build)-pkg-config
    edo sed -i -e "/AC_PATH_PROG(BUILD_PKG_CONFIG/d" gtk/configure.ac

    # Use the correct compilers, honor debug info flags
    edo sed -e "/^GCC.gcc *=/s:= gcc:= $(exhost --tool-prefix)cc:" \
            -e "/^GCC.gxx *=/s:= .*:= $(exhost --tool-prefix)c++:" \
            -e "/^GCC.args.g.none *=/s:-g0::" \
            -e "/^GCC.args.strip *=/s:-Wl,-S::" \
            -i make/*/*.defs

    # Looks like we are the first ones to use --cross without mingw :|
    # https://github.com/HandBrake/HandBrake/pull/606
    edo sed -e "s/self.systemf\[0\] = self.systemf\[0\].upper()/self.systemf = self.systemf.capitalize()/" \
            -i make/configure.py

    # undefined reference to symbol 'x265_api_query'
    edo sed -i -e '/USE_X265/a\
    TEST.GCC.l += x265' test/module.defs

    # fix localization search path for multilib
    edo sed \
        -e 's:$(prefix):/usr:g' \
        -i gtk/src/Makefile.am
}

src_configure() {
    edo python2.7 make/configure.py \
        --cross="$(exhost --target)" \
        --gcc=cc \
        --pkg-config="${PKG_CONFIG}" \
        --force \
        --prefix="/usr/$(exhost --target)" \
        --enable-fdk-aac \
        --disable-df-fetch \
        --disable-df-verify \
        --disable-gst \
        --disable-gtk-update-checks \
        --disable-libav-aac \
        $(option_enable hevc x265) \
        $(option_enable gtk3 gtk)
}

src_compile() {
    emake -C build
}

src_install() {
    emake -C build DESTDIR="${IMAGE}" install

    emagicdocs

    if [[ -d "${IMAGE}"/usr/$(exhost --target)/share ]]; then
        edo mv "${IMAGE}"/usr/$(exhost --target)/share/* "${IMAGE}"/usr/share/
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
    fi
}

pkg_postinst() {
    if option gtk3 ; then
        freedesktop-desktop_pkg_postinst
        gtk-icon-cache_pkg_postinst
    fi
}

pkg_postrm() {
    if option gtk3 ; then
        freedesktop-desktop_pkg_postrm
        gtk-icon-cache_pkg_postrm
    fi
}

