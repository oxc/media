# Copyright 2016-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=MaartenBaert pn=ssr ] \
    cmake [ api=2 ] \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="SimpleScreenRecorder, a screen recorder for Linux"
DESCRIPTION="
* Graphical user interface (Qt-based).
* Faster than VLC and ffmpeg/avconv.
* Records the entire screen or part of it, or records OpenGL applications directly (similar to
  Fraps on Windows).
* Synchronizes audio and video properly (a common issue with VLC and ffmpeg/avconv).
* Reduces the video frame rate if your computer is too slow (rather than using up all your RAM
  like VLC does).
* Fully multithreaded: small delays in any of the components will never block the other components,
  resulting is smoother video and better performance on computers with multiple processors.
* Pause and resume recording at any time (either by clicking a button or by pressing a hotkey).
* Shows statistics during recording (file size, bit rate, total recording time, actual frame rate, ...).
* Can show a preview during recording, so you don't waste time recording something only to figure
  out afterwards that some setting was wrong.
* Uses libav/ffmpeg libraries for encoding, so it supports many different codecs and file formats
  (adding more is trivial).
* Can also do live streaming (experimental).
* Sensible default settings: no need to change anything if you don't want to.
* Tooltips for almost everything: no need to read the documentation to find out what something
  does.
"
HOMEPAGE+=" http://www.maartenbaert.be/${PN}"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    alsa
    jack
    pulseaudio
    ( alsa jack pulseaudio ) [[ number-selected = at-least-one ]]
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
    build+run:
        x11-dri/glu
        x11-dri/mesa [[ note = [ provides libGL ] ]]
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/qtbase:5[gui]
        x11-libs/qtx11extras:5
        alsa? ( sys-sound/alsa-lib )
        jack? ( media-sound/jack-audio-connection-kit )
        providers:ffmpeg? ( media/ffmpeg[h264][vorbis] )
        providers:libav? ( media/libav[h264][vorbis] )
        pulseaudio? ( media-sound/pulseaudio )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DENABLE_32BIT_GLINJECT:BOOL=FALSE
    -DENABLE_FFMPEG_VERSIONS:BOOL=TRUE
    -DENABLE_X86_ASM:BOOL=TRUE
    -DWITH_GLINJECT:BOOL=TRUE
    -DWITH_OPENGL_RECORDING:BOOL=TRUE
    -DWITH_QT5:BOOL=TRUE
    -DWITH_SIMPLESCREENRECORDER:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'alsa ALSA'
    'jack JACK'
    'pulseaudio PULSEAUDIO'
)

src_prepare() {
    cmake_src_prepare

    # Fix running lrelease to get translations
    edo sed \
        -e 's:lrelease:lrelease-qt5:g' \
        -i src/translations/CMakeLists.txt
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

